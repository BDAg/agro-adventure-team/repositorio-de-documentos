# Agro Adventure - Documentos
**[Clique para ir para o repositório do Jogo](https://gitlab.com/BDAg/agro-adventure-team/AgroAdventure)**

**[Acesse aqui nosso Wiki](https://gitlab.com/BDAg/agro-adventure-team/repositorio-de-documentos/-/wikis/home)** para ver nosso time desenvolvimento e o andamento do projeto

**[Você pode baixar a primeira versão do jogo clicando aqui](https://drive.google.com/drive/folders/13W8IMiMpCi0AtFauY08aMF0GEfAOKtIQ?usp=sharing)** (Baixe a o arquivo AgroAdventure.zip, estraia a pasta e execute o arquivo AgroAdventure.exe)